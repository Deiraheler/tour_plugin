<?php
add_action('save_post', 'savePost');
function savePost(){
    if(isset($_POST['description'])){
        update_post_meta( $_POST['post_id'], 'description', $_POST['description'] );
    }

    if(isset($_POST['cost'])){
        update_post_meta( $_POST['post_id'], 'cost', $_POST['cost'] );
    }

    if(isset($_POST['country'])){
        update_post_meta( $_POST['post_id'], 'country', $_POST['country'] );
    }
    
    if(isset($_POST['images'])){
        update_post_meta( $_POST['post_id'], 'images', $_POST['images'] );
    }
    
    if(isset($_POST['time_start'])){
        update_post_meta( $_POST['post_id'], 'time_start', $_POST['time_start'] );
    }

    if(isset($_POST['time_end'])){
        update_post_meta( $_POST['post_id'], 'time_end', $_POST['time_end'] );
    }

    // echo "<script>window.location.href='" . admin_url('/post.php?post=' . $_POST['post_id'] . '&action=edit') . "'</script>"; 
}

function tour_add_fields(){
$plugin_dir = ABSPATH . 'wp-content/plugins/tour_plugin/';
$allCountries = ['Абхазия','Австралия','Австрия','Азербайджан','Албания','Алжир','Ангола','Андорра','Антигуа и Барбуда','Аргентина','Армения','Афганистан','Багамские Острова','Бангладеш','Барбадос','Бахрейн','Белиз','Белоруссия','Бельгия','Бенин','Болгария','Боливия','Босния и Герцеговина','Ботсвана','Бразилия','Бруней','Буркина-Фасо','Бурунди','Бутан','Вануату','Ватикан','Великобритания','Венгрия','Венесуэла','Восточный','Тимор','Вьетнам','Габон','Гаити','Гайана','Гамбия','Гана','Гватемала','Гвинея','Гвинея-Бисау','Германия','Гондурас','Государство Палестина','Гренада','Греция','Грузия','Дания','Джибути','Доминика','Доминиканская Республика','ДР Конго','Египет','Замбия','Зимбабве','Израиль','Индия','Индонезия','Иордания','Ирак','Иран','Ирландия','Исландия','Испания','Италия','Йемен','Кабо-Верде','Казахстан','Камбоджа','Камерун','Канада','Катар','Кения','Кипр','Киргизия','Кирибати','Китай','КНДР','Колумбия','Коморские Острова','Коста-Рика','Кот-д\'Ивуар','Куба','Кувейт','Лаос','Латвия','Лесото','Либерия','Ливан','Ливия','Литва','Лихтенштейн','Люксембург','Маврикий','Мавритания','Мадагаскар','Малави','Малайзия','Мали','Мальдивские Острова','Мальта','Марокко','Маршалловы Острова','Мексика','Мозамбик','Молдавия','Монако','Монголия','Мьянма','Намибия','Науру','Непал','Нигер','Нигерия','Нидерланды','Никарагуа','Новая Зеландия','Норвегия','ОАЭ','Оман','Пакистан','Палау','Панама','Папуа - Новая','Гвинея','Парагвай','Перу','Польша','Португалия','Республика Конго','Республика Корея','Россия','Руанда','Румыния','Сальвадор','Самоа','Сан-Марино','Сан-Томе и Принсипи','Саудовская Аравия','Северная Македония','Сейшельские Острова','Сенегал','Сент-Винсент и Гренадины','Сент-Китс и Невис','Сент-Люсия','Сербия','Сингапур','Сирия','Словакия','Словения','Соломоновы Острова','Сомали','Судан','Суринам','США','Сьерра-Леоне','Таджикистан','Таиланд','Танзания','Того','Тонга','Тринидад и Тобаго','Тувалу','Тунис','Туркмения','Турция','Уганда','Узбекистан','Украина','Уругвай','Федеративные Штаты Микронезии','Фиджи','Филиппины','Финляндия','Франция','Хорватия','ЦАР','Чад','Черногория','Чехия','Чили','Швейцария','Швеция','Шри-Ланка','Эквадор','Экваториальная Гвинея','Эритрея','Эсватини','Эстония','Эфиопия','ЮАР','Южная Осетия','Южный Судан','Ямайка','Япония'];

?>
    <style>
    .fields_header{
        background-color: blue;
        color: white;
        padding: 10px;
        font-weight: bold;
        font-size: 20px;
    }
    .form-group{
        display: flex;
        flex-direction: column;
    }
    .form-group lable{
        font-size: 16px;
        color: black;
        padding: 5px 0;
    }
    .fields_content{
        padding: 5px;
        background-color: #9eb8ff;
    }
    .time-form{
        display: flex;
    }
    .time-form .form-group{
        max-width: 330px;
    }
    .small_imgs{
        width: 230px;
        padding: 3px;
        position: relative;
    }
    .images-small{
        display: flex;
        flex-wrap: wrap;
        margin: 10px 0;
    }
    .img-closer{
        display: inline;
        position: absolute;
        font-weight: bold;
        color: white;
        border-radius: 5px;
        padding: 5px 3px 3px 4px;
        line-height: 1;
        background-color: black;
        top: 10px;
        right: 15px;
        transform: scaleX(1.5);
        width: -46px;
        font-family: monospace;
        font-size: 16px;
        cursor: pointer;
    }
    #add-image{
        width: 200px;
        margin: 0 auto;
        border: 0;
        background-color: #3535ff;
        padding: 5px;
        color: white;
    }
    .button-sub{
        border: 0;
        padding: 10px;
        background-color: #09ff49;
    }
    @media screen and (max-width: 680px){
        .time-form{
            flex-direction: column;
        }
        .time-form .form-group{
            max-width: 100%;
        }
    }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>

    <div id="form_tour" style="margin-top: 30px">
        <div class="fields_header">Заполните информацию</div>
        <div class="fields_content">
            <form method="post" action="<?= admin_url('/admin-post.php') ?>"enctype="multipart/form-data">
            <?php 
                $poste = get_post();
            ?>
                <input type="hidden" name="post_id" value="<?= $poste->ID ?>">
                <div class="form-group">
                    <lable id="description">Описание</lable>
                    <textarea name="description" id="description" v-model="description"></textarea>
                </div>
                <div class="form-group">
                    <lable id="country">Страна</lable>
                    <select name="country" v-model="selected" id="country">
                        <?php 
                            foreach ($allCountries as $country){           
                                echo '<option value="' . $country . '">' . $country . '</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group" style="max-width: 400px">
                    <lable id="cost">Стоимость</lable>
                    <input type="text" name="cost" id="cost" v-model.number="cost"></input>
                </div>
                <div class="time-form">
                    <div class="form-group">
                        <lable id="time_start">Дата отправки:</lable>
                        <input type="date" name="time_start" :value="time_start" id="time_start">
                    </div>
                    <div class="form-group">
                        <lable id="time_end">Дата прибытия:</lable>
                        <input type="date" name="time_end" :value="time_end" id="time_end">
                    </div>
                </div>
                <div class="form-group">
                <input type="hidden" name="images" v-model="imgs">
                <div class="images-small">
                    <img-closer v-for="(img, index) in imgs" :key="index" @delete="deleteItem(index)" :src="img"></img-closer>
                </div>
                <button id="add-image">Добавить картинку</button>
                </div>
            </form>
        </div>
    </div>

<script>
    var app = new Vue({
    el: '#form_tour',
    data: {
        imgs: [],
        cost: 0,
        selected: '<?= get_post_meta($poste->ID, 'country')[0] ?>',
        time_start: '<?= get_post_meta($poste->ID, 'time_start')[0] ?>',
        time_end: '<?= get_post_meta($poste->ID, 'time_end')[0] ?>',
        description: '<?= get_post_meta($poste->ID, 'description')[0] ?>',
    },
    mounted(){
        let images = "<?= implode('\',\'' , explode(',' , get_post_meta($poste->ID, 'images')[0]))?>";
        if(images != ''){
            this.imgs = images.split('\',\'');
        }
        let cost = <?= intval(get_post_meta($poste->ID, 'cost')) ?>;
        if(isset(cost)){
            this.cost = cost;
        }else{
            this.cost = 0;
        }
    },
    methods: {
        deleteItem: function(index){
            this.imgs.splice(index, 1);
        }
    },
    })

    Vue.component('img-closer',{
        props: ['src','key'],
        data: function(){
            return {
                img: 0,
            }
        },
        methods: {
            deleteImg: function(){
                this.$emit('delete', this.key)
            },
        },
        template: '<div class="small_imgs"><div class="img-closer" @click="deleteImg">X</div><img style="width: 100%;" :src="src"></div>',
    })

    $('#add-image').click(function(e) {
        e.preventDefault();
        var image = wp.media({ 
            title: 'Upload Image',
            multiple: false
        }).open()
        .on('select', function(e){
            var uploaded_image = image.state().get('selection').first();
            console.log(uploaded_image);
            var image_url = uploaded_image.toJSON().url;
            // console.log(image_url);
            app.imgs.push(image_url);
        });
    });
</script>
<?php
}
?>