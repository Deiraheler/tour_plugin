<?php
/**
 * Plugin Name: Tours Plugin
 * Description: ТЗ. Плагин для создания туров
 * Author:      Dima Severin 
 * Version:     1.0.0
 */

register_activation_hook( __FILE__, 'tourActivation');
register_deactivation_hook(__FILE__, 'tourDeactivation');
// register_uninstall_hook();

add_action( 'current_screen', 'wpkama_widgets_screen' );
function wpkama_widgets_screen(){

	$screen = get_current_screen();

	if( 'tour' === $screen->id  || 'post' !== $screen->id){
		add_action( 'edit_form_after_editor', 'tour_add_fields' );
		require_once 'single-admin.php';
	}
}

function tourActivation(){
    tourSetPostType();
    // Clear the permalinks after the post type has been registered.
    flush_rewrite_rules();
} 

function tourSetPostType(){
    register_post_type( 'tour', [
		'label'  => 'tour',
		'labels' => [
			'name'               => 'Все туры', // основное название для типа записи
			'singular_name'      => 'Все туры', // название для одной записи этого типа
			'add_new'            => 'Новый Тур', // для добавления новой записи
			'add_new_item'       => 'Добавление тур', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование тур', // для редактирования типа записи
			'new_item'           => 'Новый тур', // текст новой записи
			'view_item'          => 'Смотреть тур', // для просмотра записи этого типа.
			'search_items'       => 'Искать тур', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'menu_name'          => 'Туры', // название меню
		],
		'description'         => '',
		'public'              => true,
		'show_ui'             => true, // зависит от public
		'show_in_nav_menus'   => true, // зависит от public
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'show_in_admin_bar'   => true, // зависит от show_in_menu
		'show_in_rest'        => null, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => null,
		'menu_icon'           => null,
        'hierarchical'        => false,
		'supports'            => [ 'title','thumbnail' ], // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => [],
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	] );
}
add_action( 'init', 'tourSetPostType' );


add_filter('manage_tour_posts_columns', 'bs_event_table_head');
function bs_event_table_head( $defaults ) {
    $defaults['country'] = 'Страна';
    $defaults['start_tour'] = 'Начало тура';
    $defaults['end_tour'] = 'Конец тура';
	$defaults['cost'] = 'Стоимость';
	unset($defaults['date']);
    return $defaults;
}

add_action( 'manage_tour_posts_custom_column', 'bs_event_table_content', 10, 2 );

function bs_event_table_content( $column_name, $post_id ) {
    if ($column_name == 'country') {
	$country = get_post_meta( $post_id, 'country', true);
	echo $country;
    }
    if ($column_name == 'start_tour') {
    $start_tour = get_post_meta( $post_id, 'time_start', true );
    echo $start_tour;
	}
	if ($column_name == 'end_tour') {
		$end_tour = get_post_meta( $post_id, 'time_end', true );
		echo $end_tour;
		}

    if ($column_name == 'cost') {
    	echo get_post_meta( $post_id, 'cost', true );
    }

}